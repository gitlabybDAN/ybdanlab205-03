using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendTree2D : MonoBehaviour
{
    Animator anim;
    public float sideSpeed;
    public float forwardSpeed;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) && forwardSpeed < 0.5) // Walk straight if UpArrow 'OR' W pressed
        {
            forwardSpeed += Time.deltaTime * 0.1f;
            Debug.Log("walk");
        }
        if ((!Input.GetKey(KeyCode.LeftShift) && forwardSpeed > 0.5) || (!Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.W)) && forwardSpeed > 0) // Stop walking if UpArrow 'OR' W released
        {
            forwardSpeed -= Time.deltaTime * 0.2f;
            Debug.Log("stop");
        }

        if (Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) && forwardSpeed < 1) // Run if LeftShift is pressed with Uparrow 'OR' W key
        {
            forwardSpeed += Time.deltaTime * 0.75f;
            Debug.Log("run");
        }

        if (!Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) && sideSpeed > -0.5) // Walk left if LeftArrow 'OR' A pressed
        {
            sideSpeed -= Time.deltaTime * 0.1f;
            Debug.Log("left-walk");
        }
        if (!Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) && sideSpeed < 0.5) // Walk right if RightArrow 'OR' D pressd
        {
            sideSpeed += Time.deltaTime * 0.1f;
            Debug.Log("right-walk");
        }

        if (sideSpeed > 0 && ((!Input.GetKey(KeyCode.LeftShift) && sideSpeed > 0.5) || (!Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.D)))) // Slows down running right if LeftShift is released but RightArrow 'OR' D is still pressing
        {
            sideSpeed -= Time.deltaTime * 0.2f;
            Debug.Log("stop");
        }
        else if (sideSpeed < 0 && ((!Input.GetKey(KeyCode.LeftShift) && sideSpeed < -0.5) || (!Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.A)))) // Slows down running left if LeftShit is released but LeftArrow 'OR' A is still pressing
        {
            sideSpeed += Time.deltaTime * 0.2f;
            Debug.Log("stop");
        }

        if (Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) && sideSpeed > -1) // Run left if LeftShift is pressed with LeftArrow 'OR' A key
        {
            sideSpeed -= Time.deltaTime * 0.75f;
            Debug.Log("left-run");
        }
        if (Input.GetKey(KeyCode.LeftShift) && (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) && sideSpeed < 1) // Run right if LeftShift is pressed with RightArrow 'OR' D key
        {
            sideSpeed += Time.deltaTime * 0.75f;
            Debug.Log("right-run");
        }

        anim.SetFloat("VelocityX", sideSpeed); // float for left and right arrow : A and D key (negative value for left and A, positive value for right and D. limited between -1 and 1)
        anim.SetFloat("VelocityZ", forwardSpeed); // float for up arrow : W key (limited till 1)
    }
}

