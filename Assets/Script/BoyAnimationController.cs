using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoyAnimationController : MonoBehaviour
{
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space)) // Input Space key to jump
        {
            anim.SetBool("IsJumping", true);
            Debug.Log("jump");
        }
        else // Any state if user release Space key: Return to the Blend Tree 
        {
            anim.SetBool("IsJumping", false);
        }
    }
}
